public class FindPrimes {
    public static void main(String[] args) {
        int number = Integer.parseInt(args[0]);

        boolean first = true;
        int i = 2;
        while (i < number) {
            int j = 2;
            boolean divided = false;
            while (j < i & !divided) {
                if (i % j == 0)
                    divided = true;
                j++;
            }
            if (first & !divided) {
                first = false;
                System.out.print(i);
            } else if (!divided)
                System.out.print("," + i);
            i++;
        }
    }
}
